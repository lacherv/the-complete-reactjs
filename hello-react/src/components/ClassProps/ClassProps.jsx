import React, { Component } from 'react';
class ClassProps extends Component {
    render() {
        const { personName, age } = this.props;
        return (
            <div>
                <h1>
                    Hello I am {personName} and {age} years old.
                </h1>
                {this.props.children}
            </div>
        )
    }
}

export default ClassProps;