import React, { Component } from "react";

class BindEvent extends Component {
    constructor () {
        super();

        this.state = {
            message : "I am learning ReactJS."
        };

        // this.messageHandler = this.messageHandler.bind(this)
    }
    // messageHandler() {
    //     this.setState({
    //         message : "ReactJS is amazing"
    //     });
    //     console.log(this);
    // }

    messageHandler = () => {
        this.setState({
            message: "React is amazing 🚀"
        });
    }
    render() {
        return(
            <div>
                <p>{this.state.message}</p>
                {/* <button onClick={this.messageHandler.bind(this)}>Click Me!</button> */} {/* Method 1 */}
                {/* <button onClick={() => this.messageHandler()}>
                    Click Me!
                </button> */} {/* Method 2 */}
                {/* <button onClick={this.messageHandler}> Click Me!</button> */} {/* Method 3 */}
                <button onClick={this.messageHandler}> Click Me!</button> {/* Method 4 */}

            </div>
        )
    }
}

export default BindEvent;