import React from 'react'

// const FunctionalProps = ({personName, age}) => {
//     return (
//          <div>
//             <h1>Hello, I am {personName}, and {age} years old.</h1>
//             {/* {children} */}
//         </div>
//     )
// }
const FunctionalProps = (props) => {
    const { personName, age } = props;
    return (
        <div>
            <h1>Hello, I am {personName}, and {age} years old.</h1>
            {/* {children} */}
        </div>
    )
}

export default FunctionalProps;