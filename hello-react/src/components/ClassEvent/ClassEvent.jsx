import React, { Component } from 'react';

class ClassEvent extends Component {
    mouseOver() {
        console.log("You hovered over me!");
    }
    render() {
        return (
            <div>
                <p onMouseOver= {this.mouseOver}>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Obcaecati numquam voluptatum, sequi libero corrupti
                    totam doloribus eaque vero recusandae dignissimos optio
                    asperiores laboriosam animi esse, nobis in. Doloremque,
                    quidem necessitatibus.
                </p>
            </div>
        )
    }
}

export default ClassEvent;
