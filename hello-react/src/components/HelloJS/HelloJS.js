import React from 'react';

const HelloJS = () => {
    return React.createElement(
        'div', {
            className: "js-component", style: {backgroundColor: "crimson"}
        },
        // '<h1>Hello fron JS. I am a JS component.</h1>'
        React.createElement(
            'h1',
            null,
            'Hello fron JS. I am a JS component.'
        )
    )
}

export default HelloJS