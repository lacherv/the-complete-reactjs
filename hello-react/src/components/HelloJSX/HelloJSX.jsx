import React from 'react';

const HelloJSX = () => {
    return (
        <h1 className='js-component' style={{ backgroundColor: "blue", color: "white" }}>
            Hello from JSX. I am JSX component!
        </h1>
    )
}

export default HelloJSX;