import React from 'react';

const FunctionalEvent = () => {
    const clickEvent = () => {
        console.log("Click from the functional component 👇")
    }
    return (
        <div>
            <p>Button from FunctionalEvent component.</p>
            <button onClick={clickEvent}>Click Me!</button>
        </div>
    )
}

export default FunctionalEvent;