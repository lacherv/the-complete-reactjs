import React from 'react';

function HelloFunctionalComponent() {
    return <h1>Hello, have a nice day!</h1>;
}

// const HelloFunctionalComponent = () => {
//     return <h1>Hello, have a nice day!</h1>
// }

// export  const HelloFunctionalComponent = () => <h1>Hello, have a nice day!</h1>
// export  const HiFunctionalComponent = () => <h1>Hi, have a nice day!</h1>
// export  const ByeFunctionalComponent = () => <h1>Bye, have a nice day!</h1>

export default HelloFunctionalComponent;