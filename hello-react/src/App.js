import './App.css';
import BindEvent from './components/BindEvent/BindEvent';

function App() {
  return (
    <div className="App">
      <BindEvent />
    </div>
  );
}

export default App;